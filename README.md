# PCS3539

Projeto final da disciplina PCS3539 - Tecnologia de Computação Gráfica 

# Desenvolvedores

Daniel Hiroki Yamashita (10337172)<br/>
Henrique Yukio Murata (10792153)<br/>
José Vitor Martins Makiyama (10792128)<br/>
Leonardo Ihara Ishicava (10823730)<br/>
Nicholas Yassuo Ito (10774076)

# Asylum

Asylum é um survival/psychological horror single-player em que o jogador deve escapar de um manicômio abandonado. Contando, inicialmente, apenas com uma lanterna fraca o jogador deve buscar por chaves, dentre as quais apenas uma é a correta para abrir a porta de saída, tendo que avitar um monstro que ronda os corredores estreitos do manicômio. As chaves e uma  lanterna forte são distribuídas aleatoriamente pelos quartos no início de cada partida. 


